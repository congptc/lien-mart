import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {  HttpClientModule } from '@angular/common/http';
import { ProductComponent } from './content/product/product.component';
import { CartComponent } from './content/cart/cart.component';
import { ProductAdminComponent } from './content/product/admin/product-admin.component'

const routes: Routes = [
  {path:'', component:ProductComponent},
  {path:'home', component:ProductComponent},
  {path:'cart', component:CartComponent},
  {path:'admin/products', component:ProductAdminComponent},
  {path: '**', pathMatch: 'full', redirectTo: '/'},

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    HttpClientModule,
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
