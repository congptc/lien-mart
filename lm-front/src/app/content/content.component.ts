  import { Component } from '@angular/core';

@Component({
  selector: 'content-body',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})

export class ContentComponent {
  title = 'angular-example';
}
