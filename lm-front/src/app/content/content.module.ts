
import { NgModule } from '@angular/core';
import { ContentComponent } from './content.component';
import { ProductComponent } from './product/product.component';
import { CartComponent } from './cart/cart.component';
import { AppRoutingModule } from '../app-routing.module';
import { CommonModule } from '@angular/common';
import { ProductAdminComponent } from './product/admin/product-admin.component';
import { AgGridModule } from 'ag-grid-angular';

@NgModule({
  declarations: [
    ContentComponent,
    ProductComponent,
    CartComponent,
    ProductAdminComponent,
  ],
  exports:[
    ContentComponent
  ],
  imports: [
    AppRoutingModule,
    CommonModule,
    AgGridModule.withComponents([]),
  ],
  providers: [],
})
export class ContentModule { }
