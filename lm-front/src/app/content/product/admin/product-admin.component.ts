  import { Component, OnInit } from '@angular/core';
  import {ProductService} from '../../product/product.service';

@Component({
  selector: 'products-admin',
  templateUrl: './product-admin.component.html',
  styleUrls: ['./product-admin.component.scss'],
  providers:[
    ProductService,
  ]
})

export class ProductAdminComponent implements OnInit {

  public products:any[];
  
  constructor(
    private productService:ProductService,
  ){}
  
  productColumnDefs:any[];

  
  ngOnInit(): void {
    this.buildColumn();
  }

  
 
  onGridReady(event){
   this.products = [];
   this.productService.getProductList().subscribe(
      (res)=>{
        this.products = res;
      }
    );
 }

  private buildColumn(){

    this.productColumnDefs = [
      {headerName:'Ảnh sản phẩm',field: 'img',pinned: 'left' },
      {headerName:'Tên sản phẩm',field: 'name',},
      {headerName:'Giá',field: 'price', },
      {headerName:'Mô tả',field: 'description' },
      {headerName:'Trạng thái',field: 'status' },
      {headerName:'Người tạo',field: 'createUser' },
      {headerName:'Ngày tạo',field: 'createdDate' },
    ];

  }


}
