  import { Component, OnInit } from '@angular/core';
  import {ProductService} from '../product/product.service';

@Component({
  selector: 'products',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
  providers:[
    ProductService,
  ]
})

export class ProductComponent implements OnInit {

  public products:any[];
  
  constructor(
    private productService:ProductService,
  ){}

  
  ngOnInit(): void {
    this.getProductList();
  }

  
  private getProductList(){
    this.productService.getProductList().subscribe(
      (res)=>{
        this.products = res;
      }
    );
  }


}
