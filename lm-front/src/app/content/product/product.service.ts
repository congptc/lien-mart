import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';

import {Product} from './product.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ProductService {

  selectedProduct : Product;
  products: Product[];

  readonly baseUrl = environment.apiUrl+'/products';

  constructor(private http: HttpClient) { }


  getProducts():any{
        return [
            {name:'Sản phẩm 1',price:'100000',img:'Caykay.jpg'},
            {name:'Sản phẩm 1',price:'100000',img:'Caykay.jpg'},
            {name:'Sản phẩm 1',price:'100000',img:'Caykay.jpg'},
            {name:'Sản phẩm 1',price:'100000',img:'Caykay.jpg'},
            {name:'Sản phẩm 1',price:'100000',img:'Caykay.jpg'},
            {name:'Sản phẩm 1',price:'100000',img:'Caykay.jpg'},
        ]
  }

  getProductList(): Observable<any>{
      return this.http.get(this.baseUrl);
  }

}